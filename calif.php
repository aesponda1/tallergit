<?php
$titulo = 'Calificaciones --- Principios de Termodin&aacute;mica y Electromagnetismo';

include 'cabecera151.php';
?>
<style type="text/css">
<!--
.style1 {color: #FF0000}
.style3 {color: #FF0000; font-weight: bold; }
-->
</style>


<h2>Calificaciones Finales</h2>
<table cellspacing="0" cols="17" border="0">
<tr>
    <th width="200" align="center" rowspan="2" valign="middle" bgcolor="#CC99FF" >No. de Cuenta</th>
      <th width="42" align="center" rowspan="2" valign="middle" bgcolor="#CC99FF" >Prom Asist.</th>
      <th width="47" align="center" rowspan="2" valign="middle" bgcolor="#CC99FF" >Prom Tareas</th>
      <th width="65" align="center" rowspan="2" valign="middle" bgcolor="#CC99FF">Prom T.Equipo</th>
      <th colspan="3" align="center" valign="middle" bgcolor="#CC99FF" >Parciales</th>
      <th width="61" align="center" rowspan="2" valign="middle" bgcolor="#CC99FF" >Prom Parciales</th>
      <th width="56" rowspan="2" align="center" valign="middle" bgcolor="#CC99FF">Labora-torio</th>
      <th width="60" rowspan="2" align="center" valign="middle" bgcolor="#CC99FF">Proyecto</th>
      <th colspan="2" align="center" valign="middle" bgcolor="#CC99FF">Taller Ejercicios</th>
      <th width="61" rowspan="2" align="center" valign="middle" bgcolor="#CC99FF" ><strong>Apuntes Clase</th>
      <th colspan="2" align="center" valign="middle" bgcolor="#CC99FF" ><strong>Finales</strong></th>
    <th rowspan="2" width="55" align="center" valign="middle" bgcolor="#CC99FF" sdnum="2057;0;#;[RED]'&lt;6'#"><strong>Promedio</strong></th>
      <th rowspan="2" width="55" align="center" valign="middle" bgcolor="#CC99FF" sdnum="2057;0;#;[RED]'&lt;6'#"><strong>CAL. FINAL</strong></th>
  </tr>
    <tr>
      <td height="20" align="center" bgcolor="#CC99FF" sdnum="1033;0;#-#######-0"><strong>1</strong></td>
      <td align="center" bgcolor="#CC99FF" sdnum="1033;0;0.00"><strong>2</strong></td>
      <td align="center" bgcolor="#CC99FF" sdnum="1033;0;0.00"><strong>3</strong></td>
      <td align="center" bgcolor="#CC99FF" sdnum="1033;0;0.00"><strong>#</strong></td>
      <td align="center" bgcolor="#CC99FF" sdval="1" sdnum="1033;0;0"><strong>Ptos</strong></td>
      <td align="center" bgcolor="#CC99FF" sdval="2" sdnum="1033;0;0"><strong>1</strong></td>
      <td align="center" bgcolor="#CC99FF" sdval="3" sdnum="1033;0;0"><strong>2</strong></td>
    </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="306244575" sdnum="1033;0;#-#######-0">3-0624457-5</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">84%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">1.82<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="3.5" sdnum="1033;0;0.0">3.5</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="1.16666666666667" sdnum="1033;0;0.00">1.17</td>
      <td align="center" bgcolor="#CCFFFF" sdval="5" sdnum="1033;">5</td>
      <td align="center" bgcolor="#CCFFFF">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">19<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.25<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">FINAL</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">FINAL</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">5.0<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong><span class="style1">5</span><br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="306616956" sdnum="1033;0;#-#######-0">3-0661695-6</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">76%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">5.64<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.8<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="9.5" sdnum="1033;0;0.0">9.5</td>
      <td align="center" bgcolor="#CCFFFF" sdval="10.4" sdnum="1033;0;0.0">10.4</td>
      <td align="center" bgcolor="#CCFFFF" sdval="10" sdnum="1033;0;0.0">10.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9.725" sdnum="1033;0;0.00">9.97</td>
      <td align="center" bgcolor="#CCFFFF" sdval="7" sdnum="1033;">7</td>
      <td align="center" bgcolor="#CCFFFF">9.5<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.38<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">9.2<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>9<br />
      </strong></td>
    </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="306673757" sdnum="1033;0;#-#######-0">3-0667375-7</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">69%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">7.82<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="7" sdnum="1033;0;0.0">7.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="5.66666666666667" sdnum="1033;0;0.00">2.33</td>
      <td align="center" bgcolor="#CCFFFF" sdval="10" sdnum="1033;">10</td>
      <td align="center" bgcolor="#CCFFFF">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">3<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.04<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="6.5" sdnum="1033;0;0.00">6.50</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdval="7.2" sdnum="1033;0;0.0">7.2</td>
      <td align="center" bgcolor="#99CCFF" sdval="7" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>7</strong></td>
    </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="307016885" sdnum="1033;0;#-#######-0">3-0701688-5</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">90%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">6.64<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0.95" sdnum="1033;0;0.0">4.7</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="3.48333333333333" sdnum="1033;0;0.00">1.58</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8" sdnum="1033;">8</td>
      <td align="center" bgcolor="#CCFFFF">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="6.5" sdnum="1033;0;0.00">6.50</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdval="6.8" sdnum="1033;0;0.0">6.8</td>
      <td align="center" bgcolor="#99CCFF" sdval="7" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>7</strong></td>
    </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="307136950" sdnum="1033;0;#-#######-0">3-0713695-0</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">0%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.67<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.00">0.00</td>
      <td align="center" bgcolor="#CCFFFF">NP</td>
      <td align="center" bgcolor="#CCFFFF">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">FINAL</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">FINAL</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">NP<br /></td>
      <td align="center" bgcolor="#99CCFF" sdnum="1033;0;#,##0;[RED]-#,##0"><strong><span class="style1">NP</span><br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="307174075" sdnum="1033;0;#-#######-0">3-0717407-5</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">100%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">8.33<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.8<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="7.25" sdnum="1033;0;0.0">7.3</td>
      <td align="center" bgcolor="#CCFFFF" sdval="10" sdnum="1033;0;0.0">10.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="2.77" sdnum="1033;0;0.0">2.8</td>
      <td align="center" bgcolor="#CCFFFF" sdval="7.505" sdnum="1033;0;0.00">6.67</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9" sdnum="1033;">9</td>
      <td align="center" bgcolor="#CCFFFF">10.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">FINAL</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">8.75</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">8.8<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>9<br />
      </strong></td>
    </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="307191245" sdnum="1033;0;#-#######-0">3-0719124-5</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">38%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">1.73<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="2.1" sdnum="1033;0;0.0">2.1</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0.7" sdnum="1033;0;0.00">0.70</td>
      <td align="center" bgcolor="#CCFFFF" sdval="7" sdnum="1033;">7</td>
      <td align="center" bgcolor="#CCFFFF">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">FINAL</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">FINAL</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">NP<br /></td>
      <td align="center" bgcolor="#99CCFF" sdnum="1033;0;#,##0;[RED]-#,##0"><strong><span class="style1">NP</span><br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="307330543" sdnum="1033;0;#-#######-0">3-0733054-3</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">100%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">7.36<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="6" sdnum="1033;0;0.0">6.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="10.95" sdnum="1033;0;0.0">11.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="7.22" sdnum="1033;0;0.0">7.2</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8.4175" sdnum="1033;0;0.00">8.06</td>
      <td align="center" bgcolor="#CCFFFF" sdval="7" sdnum="1033;">7</td>
      <td align="center" bgcolor="#CCFFFF">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="7.5" sdnum="1033;0;0.00">7.50</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdval="7.4" sdnum="1033;0;0.0">7.4</td>
      <td align="center" bgcolor="#99CCFF" sdval="7" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>7</strong></td>
    </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="308032965" sdnum="1033;0;#-#######-0">3-0803296-5</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">17%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.73<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.00">0.00</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8" sdnum="1033;">8</td>
      <td align="center" bgcolor="#CCFFFF">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">FINAL</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">FINAL</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">NP<br /></td>
      <td align="center" bgcolor="#99CCFF" sdnum="1033;0;#,##0;[RED]-#,##0"><strong><span class="style1">NP</span><br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="308211564" sdnum="1033;0;#-#######-0">3-0821156-4</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">97%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.73<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="10.5" sdnum="1033;0;0.0">10.5</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9.9" sdnum="1033;0;0.0">9.9</td>
      <td align="center" bgcolor="#CCFFFF" sdval="7.22" sdnum="1033;0;0.0">7.2</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9.155" sdnum="1033;0;0.00">9.21</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8" sdnum="1033;">8</td>
      <td align="center" bgcolor="#CCFFFF">9.5<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.48<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">8.8<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>9<br />
      </strong></td>
    </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="308687307" sdnum="1033;0;#-#######-0">3-0868730-7</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">100%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">8.91<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.8<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="7.2" sdnum="1033;0;0.0">7.2</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9.85" sdnum="1033;0;0.0">9.9</td>
      <td align="center" bgcolor="#CCFFFF" sdval="6.11" sdnum="1033;0;0.0">6.1</td>
      <td align="center" bgcolor="#CCFFFF" sdval="7.915" sdnum="1033;0;0.00">7.72</td>
      <td align="center" bgcolor="#CCFFFF" sdval="7" sdnum="1033;">7</td>
      <td align="center" bgcolor="#CCFFFF">10<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">2<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.03<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="7.5" sdnum="1033;0;0.00">7.50</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">8.4<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>8<br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="309000367" sdnum="1033;0;#-#######-0">3-0900036-7</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">79%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">4.36<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.5<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="4.7" sdnum="1033;0;0.0">4.7</td>
      <td align="center" bgcolor="#CCFFFF" sdval="5.85" sdnum="1033;0;0.0">5.9</td>
      <td align="center" bgcolor="#CCFFFF" sdval="6.66" sdnum="1033;0;0.0">6.7</td>
      <td align="center" bgcolor="#CCFFFF" sdval="6.4275" sdnum="1033;0;0.00">5.74</td>
      <td align="center" bgcolor="#CCFFFF" sdval="7" sdnum="1033;">7</td>
      <td align="center" bgcolor="#CCFFFF">9.5<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="6.3" sdnum="1033;0;0.00">6.30</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">6.9<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>7<br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="309007335" sdnum="1033;0;#-#######-0">3-0900733-5</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">59%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">2.64<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="6.7" sdnum="1033;0;0.0">6.7</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="2.23333333333333" sdnum="1033;0;0.00">2.23</td>
      <td align="center" bgcolor="#CCFFFF" sdval="6" sdnum="1033;">6</td>
      <td align="center" bgcolor="#CCFFFF">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">FINAL</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">FINAL</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">NP<br /></td>
      <td align="center" bgcolor="#99CCFF" sdnum="1033;0;#,##0;[RED]-#,##0"><strong><span class="style1">NP</span><br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="309023522" sdnum="1033;0;#-#######-0">3-0902352-2</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">100%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">7.64<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="6.2" sdnum="1033;0;0.0">6.2</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8.9" sdnum="1033;0;0.0">8.9</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9.44" sdnum="1033;0;0.0">9.4</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8.385" sdnum="1033;0;0.00">8.18</td>
      <td align="center" bgcolor="#CCFFFF" sdval="6" sdnum="1033;">6</td>
      <td align="center" bgcolor="#CCFFFF">9.5<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">6.5<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>6<br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="309037475" sdnum="1033;0;#-#######-0">3-0903747-5</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">55%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">5.27<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="6.7" sdnum="1033;0;0.0">6.7</td>
      <td align="center" bgcolor="#CCFFFF" sdval="6" sdnum="1033;0;0.0">6.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="2.22" sdnum="1033;0;0.0">2.2</td>
      <td align="center" bgcolor="#CCFFFF" sdval="3.73" sdnum="1033;0;0.00">4.97</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9" sdnum="1033;">9</td>
      <td align="center" bgcolor="#CCFFFF">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">FINAL</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">FINAL</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">NP<br /></td>
      <td align="center" bgcolor="#99CCFF" sdnum="1033;0;#,##0;[RED]-#,##0"><strong><span class="style1">NP</span><br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="309042105" sdnum="1033;0;#-#######-0">3-0904210-5</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">97%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">7.09<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.8<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="11" sdnum="1033;0;0.0">11.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="11" sdnum="1033;0;0.0">11.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="10" sdnum="1033;0;0.0">10.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="10.5" sdnum="1033;0;0.00">10.67</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9" sdnum="1033;">9</td>
      <td align="center" bgcolor="#CCFFFF">9.0</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">1<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.01<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.48<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">10.2<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>10<br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="309045508" sdnum="1033;0;#-#######-0">3-0904550-8</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">93%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.77<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="8.9" sdnum="1033;0;0.0">8.9</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9.9" sdnum="1033;0;0.0">9.9</td>
      <td align="center" bgcolor="#CCFFFF" sdval="11" sdnum="1033;0;0.0">11.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9.7" sdnum="1033;0;0.00">9.93</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9" sdnum="1033;">9</td>
      <td align="center" bgcolor="#CCFFFF">9.0</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">1<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.01<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.47<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">8.9<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>9<br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="309096029" sdnum="1033;0;#-#######-0">3-0909602-9</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">3%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.91<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.00">0.00</td>
      <td align="center" bgcolor="#CCFFFF" sdval="7" sdnum="1033;">7</td>
      <td align="center" bgcolor="#CCFFFF">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">FINAL</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">FINAL</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">NP<br /></td>
      <td align="center" bgcolor="#99CCFF" sdnum="1033;0;#,##0;[RED]-#,##0"><strong><span class="style1">NP</span><br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="309104843" sdnum="1033;0;#-#######-0">3-0910484-3</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">100%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.82<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="6.5" sdnum="1033;0;0.0">6.5</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9.95" sdnum="1033;0;0.0">10.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="3.88" sdnum="1033;0;0.0">3.9</td>
      <td align="center" bgcolor="#CCFFFF" sdval="7.4575" sdnum="1033;0;0.00">6.78</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8" sdnum="1033;">8</td>
      <td align="center" bgcolor="#CCFFFF">9.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="6.5" sdnum="1033;0;0.00">6.50</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">7.4<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>7<br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="309119913" sdnum="1033;0;#-#######-0">3-0911991-3</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">7%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.00">0.00</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;">NP</td>
      <td align="center" bgcolor="#CCFFFF">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">FINAL</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">FINAL</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">NP<br /></td>
      <td align="center" bgcolor="#99CCFF" sdnum="1033;[RED]"><strong><span class="style1">NP</span><br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="309164339" sdnum="1033;0;#-#######-0">3-0916433-9</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">100%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">8.55<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.8<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="9" sdnum="1033;0;0.0">9.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="10.9" sdnum="1033;0;0.0">10.9</td>
      <td align="center" bgcolor="#CCFFFF" sdval="10" sdnum="1033;0;0.0">10.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9.725" sdnum="1033;0;0.00">9.97</td>
      <td align="center" bgcolor="#CCFFFF" sdval="7" sdnum="1033;">7</td>
      <td align="center" bgcolor="#CCFFFF">9.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">9.0<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>9<br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="309178196" sdnum="1033;0;#-#######-0">3-0917819-6</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">97%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.73<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="11" sdnum="1033;0;0.0">11.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9.9" sdnum="1033;0;0.0">9.9</td>
      <td align="center" bgcolor="#CCFFFF" sdval="10" sdnum="1033;0;0.0">10.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9.975" sdnum="1033;0;0.00">10.30</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8" sdnum="1033;">8</td>
      <td align="center" bgcolor="#CCFFFF">9.5<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">2<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.03<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.48<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">9.1<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>9<br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="309209601" sdnum="1033;0;#-#######-0">3-0920960-1</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">97%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">6.73<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.8<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="5.6" sdnum="1033;0;0.0">5.6</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0.9" sdnum="1033;0;0.0">4.5</td>
      <td align="center" bgcolor="#CCFFFF" sdval="6.11" sdnum="1033;0;0.0">6.1</td>
      <td align="center" bgcolor="#CCFFFF" sdval="5.4025" sdnum="1033;0;0.00">5.40</td>
      <td align="center" bgcolor="#CCFFFF" sdval="7" sdnum="1033;">7</td>
      <td align="center" bgcolor="#CCFFFF">10.0</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.48<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="7.3" sdnum="1033;0;0.00">7.30</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">7.9<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>8<br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="309239318" sdnum="1033;0;#-#######-0">3-0923931-8</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">97%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">8.80<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="8.8" sdnum="1033;0;0.0">8.8</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9.9" sdnum="1033;0;0.0">9.9</td>
      <td align="center" bgcolor="#CCFFFF" sdval="5.55" sdnum="1033;0;0.0">5.6</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8.3125" sdnum="1033;0;0.00">8.08</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8" sdnum="1033;">8</td>
      <td align="center" bgcolor="#CCFFFF">9.5<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.48<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">7.8<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>8<br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="309241164" sdnum="1033;0;#-#######-0">3-0924116-4</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">100%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.64<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.8<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="9.8" sdnum="1033;0;0.0">9.8</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9.95" sdnum="1033;0;0.0">10.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="10.55" sdnum="1033;0;0.0">10.6</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9.95" sdnum="1033;0;0.00">10.10</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9" sdnum="1033;">9</td>
      <td align="center" bgcolor="#CCFFFF">10.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.50<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">10.2<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>10<br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="309247355" sdnum="1033;0;#-#######-0">3-0924735-5</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">48%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">2.73<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="1.5" sdnum="1033;0;0.0">1.5</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0.5" sdnum="1033;0;0.00">0.50</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8" sdnum="1033;">8</td>
      <td align="center" bgcolor="#CCFFFF">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">FINAL</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">FINAL</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">5.0<br /></td>
      <td align="center" bgcolor="#99CCFF" sdnum="1033;0;#,##0;[RED]-#,##0"><strong><span class="style1">5</span><br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="309269283" sdnum="1033;0;#-#######-0">3-0926928-3</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">97%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">4.64<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">7.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="8.5" sdnum="1033;0;0.0">8.5</td>
      <td align="center" bgcolor="#CCFFFF" sdval="10" sdnum="1033;0;0.0">10.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8.33" sdnum="1033;0;0.0">8.3</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9.2075" sdnum="1033;0;0.00">8.94</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8" sdnum="1033;">8</td>
      <td align="center" bgcolor="#CCFFFF">9.5<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">2<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.03<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">8.4<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>8<br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="309271431" sdnum="1033;0;#-#######-0">3-0927143-1</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">83%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">3.41<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.5<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="6.25" sdnum="1033;0;0.0">6.3</td>
      <td align="center" bgcolor="#CCFFFF" sdval="5.85" sdnum="1033;0;0.0">5.9</td>
      <td align="center" bgcolor="#CCFFFF" sdval="3.88" sdnum="1033;0;0.0">3.9</td>
      <td align="center" bgcolor="#CCFFFF" sdval="6.12" sdnum="1033;0;0.00">5.33</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9" sdnum="1033;">9</td>
      <td align="center" bgcolor="#CCFFFF">9.5<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="7" sdnum="1033;0;0.00">7.00</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">7.5<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>8<br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="309296999" sdnum="1033;0;#-#######-0">3-0929699-9</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">100%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.27<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="7.8" sdnum="1033;0;0.0">7.8</td>
      <td align="center" bgcolor="#CCFFFF" sdval="7" sdnum="1033;0;0.0">7.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="7.77" sdnum="1033;0;0.0">7.8</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8.1425" sdnum="1033;0;0.00">7.52</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9" sdnum="1033;">9</td>
      <td align="center" bgcolor="#CCFFFF">9.5<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.50<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">7.6<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>8<br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="309306645" sdnum="1033;0;#-#######-0">3-0930664-5</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">100%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.73<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.8<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="11" sdnum="1033;0;0.0">11.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="11" sdnum="1033;0;0.0">11.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9.44" sdnum="1033;0;0.0">9.4</td>
      <td align="center" bgcolor="#CCFFFF" sdval="10.36" sdnum="1033;0;0.00">10.48</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9" sdnum="1033;">9</td>
      <td align="center" bgcolor="#CCFFFF">9.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.50<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">10.4<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>10<br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="309501297" sdnum="1033;0;#-#######-0">3-0950129-7</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">100%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">10.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.8<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="11" sdnum="1033;0;0.0">11.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9.9" sdnum="1033;0;0.0">9.9</td>
      <td align="center" bgcolor="#CCFFFF" sdval="7.77" sdnum="1033;0;0.0">7.8</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9.4175" sdnum="1033;0;0.00">9.56</td>
      <td align="center" bgcolor="#CCFFFF" sdval="10" sdnum="1033;">10</td>
      <td align="center" bgcolor="#CCFFFF">10.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.50<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">10.4<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>10<br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="309534620" sdnum="1033;0;#-#######-0">3-0953462-0</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">83%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">5.36<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.8<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="6" sdnum="1033;0;0.0">6.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="4.5" sdnum="1033;0;0.0">4.5</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8.88" sdnum="1033;0;0.0">8.9</td>
      <td align="center" bgcolor="#CCFFFF" sdval="7.095" sdnum="1033;0;0.00">6.46</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8" sdnum="1033;">8</td>
      <td align="center" bgcolor="#CCFFFF">9.5<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.41<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="6.7" sdnum="1033;0;0.00">6.70</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">7.8<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>8<br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="309567345" sdnum="1033;0;#-#######-0">3-0956734-5</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">100%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.27<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.5<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="8.5" sdnum="1033;0;0.0">8.5</td>
      <td align="center" bgcolor="#CCFFFF" sdval="10" sdnum="1033;0;0.0">10.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="10.55" sdnum="1033;0;0.0">10.6</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9.7625" sdnum="1033;0;0.00">9.68</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9" sdnum="1033;">9</td>
      <td align="center" bgcolor="#CCFFFF">9.5<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">2<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.03<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.50<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">9.9<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>10<br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="309677084" sdnum="1033;0;#-#######-0">3-0967708-4</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">48%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">5.45<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="11" sdnum="1033;0;0.0">11.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="3.66666666666667" sdnum="1033;0;0.00">3.67</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8" sdnum="1033;">8</td>
      <td align="center" bgcolor="#CCFFFF">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">FINAL</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">FINAL</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">5.0<br /></td>
      <td align="center" bgcolor="#99CCFF" sdnum="1033;0;#,##0;[RED]-#,##0"><strong><span class="style1">5</span><br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="309740203" sdnum="1033;0;#-#######-0">3-0974020-3</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">100%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.55<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.8<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="11" sdnum="1033;0;0.0">11.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="11" sdnum="1033;0;0.0">11.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="7.22" sdnum="1033;0;0.0">7.2</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9.805" sdnum="1033;0;0.00">9.74</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9" sdnum="1033;">9</td>
      <td align="center" bgcolor="#CCFFFF">9.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">9.8<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>10<br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="310003777" sdnum="1033;0;#-#######-0">3-1000377-7</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">100%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">7.45<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="6.7" sdnum="1033;0;0.0">6.7</td>
      <td align="center" bgcolor="#CCFFFF" sdval="7" sdnum="1033;0;0.0">7.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="7.22" sdnum="1033;0;0.0">7.2</td>
      <td align="center" bgcolor="#CCFFFF" sdval="7.73" sdnum="1033;0;0.00">6.97</td>
      <td align="center" bgcolor="#CCFFFF" sdval="6" sdnum="1033;">6</td>
      <td align="center" bgcolor="#CCFFFF">10.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="5.6" sdnum="1033;0;0.00">5.60</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">FINAL</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">6.4<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>6<br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="310011831" sdnum="1033;0;#-#######-0">3-1001183-1</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">100%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">7.22<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.8<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="7.25" sdnum="1033;0;0.0">7.3</td>
      <td align="center" bgcolor="#CCFFFF" sdval="10" sdnum="1033;0;0.0">10.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="6.66" sdnum="1033;0;0.0">6.7</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8.4775" sdnum="1033;0;0.00">7.97</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8" sdnum="1033;">8</td>
      <td align="center" bgcolor="#CCFFFF">10.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">8.1<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>8<br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="310013309" sdnum="1033;0;#-#######-0">3-1001330-9</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">93%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">6.45<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.5<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="4.8" sdnum="1033;0;0.0">4.8</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8.5" sdnum="1033;0;0.0">8.5</td>
      <td align="center" bgcolor="#CCFFFF" sdval="7.22" sdnum="1033;0;0.0">7.2</td>
      <td align="center" bgcolor="#CCFFFF" sdval="7.63" sdnum="1033;0;0.00">6.84</td>
      <td align="center" bgcolor="#CCFFFF" sdval="10" sdnum="1033;">10</td>
      <td align="center" bgcolor="#CCFFFF">9.5<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.47<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="7.7" sdnum="1033;0;0.00">7.70</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">8.9<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>9<br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="310026633" sdnum="1033;0;#-#######-0">3-1002663-3</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">55%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">4.36<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="3.1" sdnum="1033;0;0.0">3.1</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="1.03333333333333" sdnum="1033;0;0.00">1.03</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8" sdnum="1033;">8</td>
      <td align="center" bgcolor="#CCFFFF">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">FINAL</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">FINAL</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">5<br /></td>
      <td align="center" bgcolor="#99CCFF" sdnum="1033;0;#,##0;[RED]-#,##0"><strong><span class="style1">5</span><br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="310062132" sdnum="1033;0;#-#######-0">3-1006213-2</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">86%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">6.82<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.5<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="10.1" sdnum="1033;0;0.0">10.1</td>
      <td align="center" bgcolor="#CCFFFF" sdval="6.9" sdnum="1033;0;0.0">6.9</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8.33" sdnum="1033;0;0.0">8.3</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8.5825" sdnum="1033;0;0.00">8.44</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8" sdnum="1033;">8</td>
      <td align="center" bgcolor="#CCFFFF">9.5<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.43<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">8.3<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>8<br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="310101488" sdnum="1033;0;#-#######-0">3-1010148-8</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">100%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">8.09<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.5<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="5.4" sdnum="1033;0;0.0">5.4</td>
      <td align="center" bgcolor="#CCFFFF" sdval="5.9" sdnum="1033;0;0.0">5.9</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9.44" sdnum="1033;0;0.0">9.4</td>
      <td align="center" bgcolor="#CCFFFF" sdval="7.435" sdnum="1033;0;0.00">6.91</td>
      <td align="center" bgcolor="#CCFFFF" sdval="10" sdnum="1033;">10</td>
      <td align="center" bgcolor="#CCFFFF">9.5<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.50<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="6" sdnum="1033;0;0.00">6.00</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">8.4<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>8<br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="310127644" sdnum="1033;0;#-#######-0">3-1012764-4</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">3%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.00">0.00</td>
      <td align="center" bgcolor="#CCFFFF">NP</td>
      <td align="center" bgcolor="#CCFFFF">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">FINAL</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">FINAL</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">NP<br /></td>
      <td align="center" bgcolor="#99CCFF" sdnum="1033;0;#,##0;[RED]-#,##0"><strong><span class="style1">NP</span><br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="310142748" sdnum="1033;0;#-#######-0">3-1014274-8</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">90%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">6.50<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="3.9" sdnum="1033;0;0.0">3.9</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8.4" sdnum="1033;0;0.0">8.4</td>
      <td align="center" bgcolor="#CCFFFF" sdval="6.66" sdnum="1033;0;0.0">6.7</td>
      <td align="center" bgcolor="#CCFFFF" sdval="6.99" sdnum="1033;0;0.00">6.32</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9" sdnum="1033;">9</td>
      <td align="center" bgcolor="#CCFFFF">9.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">8<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.11<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="6" sdnum="1033;0;0.00">6.00</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">7.7<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>8<br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="310152882" sdnum="1033;0;#-#######-0">3-1015288-2</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">100%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.7<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="6.2" sdnum="1033;0;0.0">6.2</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9.95" sdnum="1033;0;0.0">10.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="5.55" sdnum="1033;0;0.0">5.6</td>
      <td align="center" bgcolor="#CCFFFF" sdval="7.8" sdnum="1033;0;0.00">7.23</td>
      <td align="center" bgcolor="#CCFFFF" sdval="10" sdnum="1033;">10</td>
      <td align="center" bgcolor="#CCFFFF">10.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">7<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.09<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.50<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="6.5" sdnum="1033;0;0.00">6.50</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">9.3<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>9<br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="310215235" sdnum="1033;0;#-#######-0">3-1021523-5</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">100%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.69<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.8<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="11" sdnum="1033;0;0.0">11.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="10" sdnum="1033;0;0.0">10.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="10" sdnum="1033;0;0.0">10.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="10.25" sdnum="1033;0;0.00">10.33</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9" sdnum="1033;">9</td>
      <td align="center" bgcolor="#CCFFFF">9.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.50<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">10.3<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>10<br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="310221357" sdnum="1033;0;#-#######-0">3-1022135-7</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">86%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">6.09<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.8<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="6.2" sdnum="1033;0;0.0">6.2</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8.2" sdnum="1033;0;0.0">8.2</td>
      <td align="center" bgcolor="#CCFFFF" sdval="5.56" sdnum="1033;0;0.0">5.6</td>
      <td align="center" bgcolor="#CCFFFF" sdval="6.74" sdnum="1033;0;0.00">6.65</td>
      <td align="center" bgcolor="#CCFFFF" sdval="10" sdnum="1033;">10</td>
      <td align="center" bgcolor="#CCFFFF">9.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">8<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.11<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.43<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">8.1<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>8<br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="310225300" sdnum="1033;0;#-#######-0">3-1022530-0</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">93%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">7.55<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="8.9" sdnum="1033;0;0.0">8.9</td>
      <td align="center" bgcolor="#CCFFFF" sdval="10" sdnum="1033;0;0.0">10.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8.33" sdnum="1033;0;0.0">8.3</td>
      <td align="center" bgcolor="#CCFFFF" sdval="6.8075" sdnum="1033;0;0.00">9.08</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8" sdnum="1033;">8</td>
      <td align="center" bgcolor="#CCFFFF">9.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">1<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.01<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">7.0<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>7<br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="310228583" sdnum="1033;0;#-#######-0">3-1022858-3</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">93%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.64<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="4.1" sdnum="1033;0;0.0">4.1</td>
      <td align="center" bgcolor="#CCFFFF" sdval="6.5" sdnum="1033;0;0.0">6.5</td>
      <td align="center" bgcolor="#CCFFFF" sdval="5.55" sdnum="1033;0;0.0">5.6</td>
      <td align="center" bgcolor="#CCFFFF" sdval="4.0375" sdnum="1033;0;0.00">5.38</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9" sdnum="1033;">9</td>
      <td align="center" bgcolor="#CCFFFF">9.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">2<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.03<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="6" sdnum="1033;0;0.00">6.00</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">6.8<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>7<br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="310279279" sdnum="1033;0;#-#######-0">3-1027927-9</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">93%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">5.23<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="10" sdnum="1033;0;0.0">10.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="6.95" sdnum="1033;0;0.0">7.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9.44" sdnum="1033;0;0.0">9.4</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8.9725" sdnum="1033;0;0.00">8.80</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8" sdnum="1033;">8</td>
      <td align="center" bgcolor="#CCFFFF">9.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.47<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">8.8<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>9<br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="310328463" sdnum="1033;0;#-#######-0">3-1032846-3</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">93%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">8.36<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="5.5" sdnum="1033;0;0.0">5.5</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9.5" sdnum="1033;0;0.0">9.5</td>
      <td align="center" bgcolor="#CCFFFF" sdval="2.22" sdnum="1033;0;0.0">2.2</td>
      <td align="center" bgcolor="#CCFFFF" sdval="6.805" sdnum="1033;0;0.00">5.74</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8" sdnum="1033;">8</td>
      <td align="center" bgcolor="#CCFFFF">10.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">5<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.07<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="5.6" sdnum="1033;0;0.00">5.60</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">7.1<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>7<br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="409029709" sdnum="1033;0;#-#######-0">4-0902970-9</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">55%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">2.60<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="6" sdnum="1033;0;0.0">6.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="5" sdnum="1033;0;0.0">5.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="3.88" sdnum="1033;0;0.0">3.9</td>
      <td align="center" bgcolor="#CCFFFF" sdval="3.72" sdnum="1033;0;0.00">4.96</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9" sdnum="1033;">9</td>
      <td align="center" bgcolor="#CCFFFF">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">FINAL</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">FINAL</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">5.0<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>5<br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="411067676" sdnum="1033;0;#-#######-0">4-1106767-6</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">97%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">3.45<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.8<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="7.5" sdnum="1033;0;0.0">7.5</td>
      <td align="center" bgcolor="#CCFFFF" sdval="11" sdnum="1033;0;0.0">11.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="7.77" sdnum="1033;0;0.0">7.8</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9.0675" sdnum="1033;0;0.00">8.76</td>
      <td align="center" bgcolor="#CCFFFF" sdval="6" sdnum="1033;">6</td>
      <td align="center" bgcolor="#CCFFFF">10.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">8.1<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>8<br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="412001909" sdnum="1033;0;#-#######-0">4-1200190-9</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">97%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.45<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.8<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="8.2" sdnum="1033;0;0.0">8.2</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9.9" sdnum="1033;0;0.0">9.9</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8.88" sdnum="1033;0;0.0">8.9</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8.995" sdnum="1033;0;0.00">8.99</td>
      <td align="center" bgcolor="#CCFFFF" sdval="7" sdnum="1033;">7</td>
      <td align="center" bgcolor="#CCFFFF">10.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.48<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">8.8<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>9<br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="412004168" sdnum="1033;0;#-#######-0">4-1200416-8</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">3%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.67<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.00">0.00</td>
      <td align="center" bgcolor="#CCFFFF">NP</td>
      <td align="center" bgcolor="#CCFFFF">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">FINAL</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">FINAL</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">NP<br /></td>
      <td align="center" bgcolor="#99CCFF" sdnum="1033;0;#,##0;[RED]-#,##0"><strong><span class="style1">NP</span><br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="412052028" sdnum="1033;0;#-#######-0">4-1205202-8</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">83%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">5.15<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="7.5" sdnum="1033;0;0.0">7.5</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9.9" sdnum="1033;0;0.0">9.9</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8.8" sdnum="1033;0;0.00">5.80</td>
      <td align="center" bgcolor="#CCFFFF" sdval="7" sdnum="1033;">7</td>
      <td align="center" bgcolor="#CCFFFF">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="7" sdnum="1033;0;0.00">7.00</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdval="7" sdnum="1033;0;0.0">7.0</td>
      <td align="center" bgcolor="#99CCFF" sdval="7" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>7</strong></td>
    </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="412055830" sdnum="1033;0;#-#######-0">4-1205583-0</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">100%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">8.35<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="11" sdnum="1033;0;0.0">11.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="10.95" sdnum="1033;0;0.0">11.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8.88" sdnum="1033;0;0.0">8.9</td>
      <td align="center" bgcolor="#CCFFFF" sdval="10.0825" sdnum="1033;0;0.00">10.28</td>
      <td align="center" bgcolor="#CCFFFF" sdval="10" sdnum="1033;">10</td>
      <td align="center" bgcolor="#CCFFFF">9.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">4<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.05<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">9.0<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>9<br />
      </strong></td>
    </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="412058831" sdnum="1033;0;#-#######-0">4-1205883-1</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">24%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">1.82<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="8.5" sdnum="1033;0;0.0">8.5</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="2.83333333333333" sdnum="1033;0;0.00">2.83</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8" sdnum="1033;">8</td>
      <td align="center" bgcolor="#CCFFFF">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">FINAL</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">FINAL</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">NP<br /></td>
      <td align="center" bgcolor="#99CCFF" sdnum="1033;0;#,##0;[RED]-#,##0"><strong><span class="style1">NP</span><br />
      </strong></td>
  </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="412059034" sdnum="1033;0;#-#######-0">4-1205903-4</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">86%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">4.82<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="2.1" sdnum="1033;0;0.0">2.1</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8.4" sdnum="1033;0;0.0">8.4</td>
      <td align="center" bgcolor="#CCFFFF" sdval="2.22" sdnum="1033;0;0.0">2.2</td>
      <td align="center" bgcolor="#CCFFFF" sdval="5.43" sdnum="1033;0;0.00">4.24</td>
      <td align="center" bgcolor="#CCFFFF" sdval="7" sdnum="1033;">7</td>
      <td align="center" bgcolor="#CCFFFF">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="5" sdnum="1033;0;0.00">5.00</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">7.38</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">7.3<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>7<br />
      </strong></td>
    </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="413005663" sdnum="1033;0;#-#######-0">4-1300566-3</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">79%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">7.64<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.8<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="6.75" sdnum="1033;0;0.0">6.8</td>
      <td align="center" bgcolor="#CCFFFF" sdval="5" sdnum="1033;0;0.0">5.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="6.11" sdnum="1033;0;0.0">6.1</td>
      <td align="center" bgcolor="#CCFFFF" sdval="4.465" sdnum="1033;0;0.00">5.95</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8" sdnum="1033;">8</td>
      <td align="center" bgcolor="#CCFFFF">9.8<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="5.8" sdnum="1033;0;0.00">5.80</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">6.7<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>7<br />
      </strong></td>
    </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="413024703" sdnum="1033;0;#-#######-0">4-1302470-3</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">100%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">8.95<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.7<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="8.5" sdnum="1033;0;0.0">8.5</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9.95" sdnum="1033;0;0.0">10.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="4.44" sdnum="1033;0;0.0">4.4</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8.0975" sdnum="1033;0;0.00">7.63</td>
      <td align="center" bgcolor="#CCFFFF" sdval="10" sdnum="1033;">10</td>
      <td align="center" bgcolor="#CCFFFF">10.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">8<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.21<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.50<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">9.9<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>10<br />
      </strong></td>
    </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="413027508" sdnum="1033;0;#-#######-0">4-1302750-8</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">100%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.45<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">9.8<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="8.15" sdnum="1033;0;0.0">8.2</td>
      <td align="center" bgcolor="#CCFFFF" sdval="11" sdnum="1033;0;0.0">11.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="8.88" sdnum="1033;0;0.0">8.9</td>
      <td align="center" bgcolor="#CCFFFF" sdval="9.5075" sdnum="1033;0;0.00">9.34</td>
      <td align="center" bgcolor="#CCFFFF" sdval="6" sdnum="1033;">6</td>
      <td align="center" bgcolor="#CCFFFF">10.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">EXENTO</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">8.9<br /></td>
      <td align="center" bgcolor="#FFFF99" sdnum="1033;0;#,##0;[RED]-#,##0"><strong>9<br />
      </strong></td>
    </tr>
    <tr>
      <td height="18" align="center" bgcolor="#CCFFFF" sdval="413085863" sdnum="1033;0;#-#######-0">4-1308586-3</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0%">0%<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.0">0.0</td>
      <td align="center" bgcolor="#CCFFFF" sdval="0" sdnum="1033;0;0.00">0.00</td>
      <td align="center" bgcolor="#CCFFFF">NP</td>
      <td align="center" bgcolor="#CCFFFF">0.0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0">0<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">0.00<br /></td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">FINAL</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.00">FINAL</td>
      <td align="center" bgcolor="#CCFFFF" sdnum="1033;0;0.0">NP<br /></td>
      <td align="center" bgcolor="#99CCFF" sdnum="1033;0;#,##0;[RED]-#,##0"><strong><span class="style1">NP</span><br />
      </strong></td>
  </tr>
</table>
<p>&nbsp;</p>      
<table border="1">
<tr><td width="10%" bgcolor="#FFFFFF">&nbsp;</td>
<td>Calificaci&oacute;n parcial tentativa</td>
<td width="10%" bgcolor="#CCFFFF">&nbsp;</td>
<td>Calificaci&oacute;n parcial definitiva</td>
<td width="10%" bgcolor="#FFFF99">&nbsp;</td>
<td>Calificaci&oacute;n final tentativa</td>
<td width="10%" bgcolor="#99CCFF">&nbsp;</td>
<td>Calificaci&oacute;n final definitiva</td>
</tr>
</table>
<p>Las calificaciones ser&aacute;n definitivas a partir de las 14 hrs del martes 10 de diciembre.  FAVOR de indicar cualquier error antes de esa hora enviando mensaje por correo electr&oacute;nico a alicia@esponda.mx o por Twitter.</p>
<p><span class="style3">Tengo un cuaderno sin nombre de alguien que asisti&oacute; a Taller de Ejercicios, el due&ntilde;o favor de indicarme su nombre describiendo su cuaderno</span>.</p>
<p>
  <?php include 'piecera141.php'; ?>
</p>

